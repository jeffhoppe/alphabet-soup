/*
This node.js program runs from the command line and takes a path and text file at argv index 2.

The algorithm is based upon how I solve these puzzles in real life, which is to iteratively scan the board's rows and columns looking for the first letter of the word, and, if found, then looking in all 8 directions (up, down, left, right, and the 4 diagonals)
trying to complete the word -- if completing the word in the chosen direction is not possible, giving up and trying a different direction, and abandoning the effort if searching goes off the bounds of the board or directions are exhausted.

A word is "found" if consecutive matching letters are found all in the same direction from the first letter -- or, rather, the number of successful iterations in the same direction matches the word's length

Sample command-line usage:
$ node.exe solver.js ./test/board1.txt
*/

// Node.js file stream
const fs = require('fs');
// argv[2]: text file path and name
const textfile = process.argv[2];

// Size of board, board array, word array
var size = { rows: 0, cols: 0 };
var board = [];
var words = [];
// Directions to search board [x, y]
const dirs = [
    [ -1, -1 ], // left, up
    [ 0, -1 ],  // up
    [ 1, -1 ],  // right, up
    [-1, 0 ],   // left
    [ 1, 0 ],   // right
    [ -1, 1 ],  // left, down
    [ 0, 1 ],   // down
    [ 1, 1 ]    // right, down
];

// Read and parse text file
// For exceptionally large files, a file stream read line-by-line might be more appropriate
var data = fs.readFileSync(textfile, { encoding: 'utf8', flag: 'r'});
parseFile(data);

// Search for words
words.forEach(word => {
    // Search board iteratively one cell at a time
    for (var x = 0; x < size.cols; x++) {
        for (var y = 0; y < size.rows; y++) {
            var res = searchAtPosition(word, x, y);
            // If res isn't false, we found the word
            if (res !== false) {
                outputWordCoords(word, res);
            }
        }
    }
});


/*
    Parses newline-separated text string into game board and parameters

    function:  parseFile
    arguments: (string) data -- string representation of board and parameters
    returns:   n/a
*/
function parseFile(data) {
    // Split on newlines/carriage returns
    var split = data.split(/\r?\n/g);

    // Parse Game board size
    var sz = split[0].split('x');
    size.rows = parseInt(sz[0]);
    size.cols = parseInt(sz[1]);

    // Construct board
    for (var i = 0; i < size.rows; i++) {
        board.push(split[i + 1].split(' '));
    }

    // Construct wordlist
    for (var i = size.rows + 1; i < split.length; i++) {
        words.push(split[i]);
    }
}

/*
    Searches board for word at position (x,y)

    function:  searchAtPosition
    arguments: (string) word -- word to be searched for
               (int) x -- x coordinate of position on board to start search
               (int) y -- y coordinate of position on board to start search
    returns:   (object) start and end coordinates of found word, false if not found
*/
function searchAtPosition(word, x, y) {
    var startcoords = { x: x, y: y };
    var endcoords = { x: null, y: null };

    // Check to see if first letter matches
    if (board[y][x] !== word.charAt(0)) return false;

    // Look in all directions for subsequent letters
    for (i = 0; i < dirs.length; i++) {
        // Get direction
        dirx = dirs[i][0];
        diry = dirs[i][1];

        // Get next board cell to check
        var cellx = x + dirx;
        var celly = y + diry;

        // If next cell is out of board bounds, skip it
        if (cellx < 0 || celly < 0 || cellx > size.cols - 1 || celly > size.rows - 1) {
            continue;
        }

        var foundlength = 1;
        // Check subsequent letters
        for (var j = 1; j < word.length; j++) {
            foundlength++;
            endcoords.x = cellx;
            endcoords.y = celly;
            // If next letter doesn't match, break
            if (board[celly][cellx] !== word.charAt(j)) break;
            // Increment cell pointer
            cellx += dirx;
            celly += diry;
        }

        // If length of found letters == word length, return start/end coordinate object
        if (word.length == foundlength) {
            return {
                start: startcoords,
                end: endcoords
            }
        }
    }
    return false;
}

function outputWordCoords(word, coords) {
    console.log(word + ' ' + coords.start.y + ':' + coords.start.x + ' ' + coords.end.y + ':' + coords.end.x);
}